package wantsome.tictactoe;

import javafx.animation.RotateTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.Reflection;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import wantsome.tictactoe.humans.Move;
import wantsome.tictactoe.humans.TicTacToeGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static javafx.scene.control.Alert.AlertType.CONFIRMATION;
import static javafx.scene.control.Alert.AlertType.INFORMATION;

/**
 * @author Amalia
 */
public class GameVsHuman extends Application {

    private static final int TILE_SIZE = 80;

    private boolean playable = true;
    private Tile[][] board;
    private int winSize;
    private List<Combo> combos = new ArrayList<>();

    private Stage stage = null;
    private Pane rootPane = new Pane();
    private Pane gamePane = new Pane();
    private Pane initPane = new Pane();
    private Button Back = new Button("BACK");
    private Button Replay = new Button("REPLAY");
    private Button quit = new Button("QUIT");
    private Label message = new Label("Win by placing five in a row!");
    private RotateTransition rotate = new RotateTransition();
    private boolean turnX = true;
    private TicTacToeGame game;


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        stage.setTitle("tic tac toe");

        createInitPaneContent();
        rootPane.getChildren().add(initPane);
        rootPane.getChildren().add(gamePane);
        primaryStage.setScene(new Scene(rootPane));
        primaryStage.show();
    }

    private void createInitPaneContent() {

        initPane.setPrefSize(700, 500);
        Text text1 = new Text();
        text1.setText("Tic Tac Toe");
        text1.setFont(Font.font(55));
        text1.setFill(Color.BLACK);
        text1.setTranslateX(200);
        text1.setTranslateY(170);
        text1.setEffect(new Reflection());
        initPane.getChildren().add(text1);

        rotate.setAxis(Rotate.Z_AXIS);
        rotate.setDuration(Duration.millis(1000));
        rotate.setToAngle(360);
        rotate.setNode(text1);
        rotate.play();

        Text text2 = new Text();
        text2.setText("Choose a board size! (3-10)");
        text2.setFont(Font.font(30));
        text2.setTextAlignment(TextAlignment.CENTER);
        text2.setFill(Color.SLATEGRAY);
        text2.setTranslateX(140);
        text2.setTranslateY(300);
        initPane.getChildren().add(text2);

        ChoiceBox<String> choiceBox = new ChoiceBox<>();
        choiceBox.setTranslateX(510);
        choiceBox.setTranslateY(270);
        choiceBox.setPrefSize(45, 45);

        for (int i = 3; i < 11; i++) {
            choiceBox.getItems().add("" + i);
        }
        choiceBox.setOnAction(e -> {
            initPane.setVisible(false);
            createGamePaneContent(Integer.parseInt(choiceBox.getValue()));
            gamePane.setVisible(true);
        });
        initPane.getChildren().addAll(choiceBox);
    }

    private void createGamePaneContent(int boardSize) {

        board = new Tile[boardSize][boardSize];
        winSize = boardSize <= 5 ? boardSize : 5;
        game = new TicTacToeGame(boardSize, winSize);

        int w = boardSize * TILE_SIZE + 200;
        int h = boardSize * TILE_SIZE + 100;

        gamePane.setLayoutX(80);
        gamePane.setLayoutY(90);

        stage.setWidth(w + 100);
        stage.setHeight(h + 100);

        Back.setTranslateX(w - 160);
        Back.setTranslateY(h / 5.5);
        Back.setPrefSize(145, 60);
        Back.setFont(Font.font("Arial", /*FontWeight.BOLD, */FontPosture.REGULAR, 20));
        gamePane.getChildren().add(Back);

        Replay.setTranslateX(w - 160);
        Replay.setTranslateY(h * 2 / 5.5);
        Replay.setPrefSize(145, 60);
        Replay.setFont(Font.font("Arial", /*FontWeight.BOLD, */FontPosture.REGULAR, 20));
        gamePane.getChildren().add(Replay);

        quit.setTranslateX(w - 160);
        quit.setTranslateY(h * 3 / 5.5);
        quit.setPrefSize(145, 60);
        quit.setFont(Font.font("Arial", /*FontWeight.BOLD, */FontPosture.REGULAR, 20));
        gamePane.getChildren().add(quit);


        message.setTranslateX(w / 4 - 100);
        message.setTranslateY(-60);
        message.setAlignment(Pos.TOP_CENTER);
        message.setFont(Font.font("Cachoeira", /*FontWeight.BOLD,*/ FontPosture.ITALIC, 30));
        gamePane.getChildren().add(message);

        if (boardSize == 3) {
            message.setText("Win by placing three in a row!");
        } else if (boardSize == 4) {
            message.setText("Win by placing four in a row!");
        } else {
            message.setText("Win by placing five in a row!");
        }

        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                Tile tile = new Tile();

                tile.setTranslateX(i * 80);
                tile.setTranslateY(j * 80);

                gamePane.getChildren().add(tile);
                board[i][j] = tile;
            }
        }

        // horizontal
        for (int row = 0; row < boardSize; row++) {
            for (int i = 0; i < boardSize - winSize + 1; i++) {
                Tile[] tiles = new Tile[winSize];
                for (int t = 0; t < winSize; t++)
                    tiles[t] = board[row][i + t];
                combos.add(new Combo(tiles));
            }
        }

        // vertical
        for (int col = 0; col < boardSize; col++) {
            for (int i = 0; i < boardSize - winSize + 1; i++) {
                Tile[] tiles = new Tile[winSize];
                for (int t = 0; t < winSize; t++)
                    tiles[t] = board[i + t][col];
                combos.add(new Combo(tiles));
            }
        }

        // diagonals
        for (int i = 0; i < boardSize - winSize + 1; i++) {
            for (int j = 0; j < boardSize - winSize + 1; j++) {
                Tile[] tiles = new Tile[winSize];
                for (int t = 0; t < winSize; t++)
                    tiles[t] = board[i + t][j + t];
                combos.add(new Combo(tiles));
            }
        }

        for (int i = 0; i < boardSize - winSize + 1; i++) {
            for (int j = 0; j < boardSize - winSize + 1; j++) {
                Tile[] tiles = new Tile[winSize];
                for (int t = 0; t < winSize; t++)
                    tiles[t] = board[i + t][boardSize - j - (t + 1)];
                combos.add(new Combo(tiles));
            }
        }

        Back.setOnAction(event -> {
            gamePane.setVisible(false);
            gamePane.getChildren().removeIf(x -> true);
            initPane.setVisible(true);
            stage.setWidth(700);
            stage.setHeight(500);
            newGame();
        });

        Replay.setOnAction(event -> {
            newGame();
        });

        quit.setOnAction(event -> {
            Alert alert = new Alert(CONFIRMATION);
            alert.setHeaderText("Are you sure you want to quit?");
            alert.setContentText("The score is: Player (X) " + game.getScorePlayerX() + " - " + "Player (O) " + game.getScorePlayerO());
            Optional<ButtonType> result = alert.showAndWait();
            if (result.orElse(null) == ButtonType.OK) {
                Platform.exit();
            }
        });

    }

    private void checkState() {
        if (!game.isMovesLeft()) {
            Alert alert = new Alert(INFORMATION);
            alert.setHeaderText("Game ended with a tie");
            alert.setContentText("SCORE: Player X (" + game.getScorePlayerX() + ")  - " + "Player O" + "(" + game.getScorePlayerO() + ")");
            alert.showAndWait();
            newGame();
        }

        for (Combo combo : combos) {
            if (combo.isComplete()) {
                playable = false;
                int winner = game.detectWinner();
                game.updateScore(winner);

                for (int i = 0; i < winSize; i++) {
                    combo.tiles[i].setColor(Color.LIGHTBLUE);
                }

                Alert alert = new Alert(INFORMATION);
                alert.initStyle(StageStyle.UTILITY);
                alert.setHeaderText("GAME OVER!");
                alert.setContentText("SCORE: Player X (" + game.getScorePlayerX() + ") - " + "Player O" + "(" + game.getScorePlayerO() + ")");
                alert.showAndWait();
            }
        }
    }

    private void newGame() {
        for (Tile[] t : board) {
            for (Tile tl : t) {
                tl.drawEmpty();
                tl.setColor(Color.AZURE);
                playable = true;
            }
        }
        game.clearBoard();
    }

    private class Combo {
        private Tile[] tiles;

        public Combo(Tile... tiles) {
            this.tiles = tiles;
        }

        public boolean isComplete() {
            if (tiles[0].getValue().equals("X") || tiles[0].getValue().equals("O")) {
                boolean result = true;
                for (int i = 0; i < winSize - 1; i++)
                    result = result && tiles[i].getValue().equals(tiles[i + 1].getValue());

                return result;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Combo{" +
                    "tiles=" + Arrays.toString(tiles) +
                    '}';
        }
    }

    private class Tile extends StackPane {

        private Text text = new Text();
        private Rectangle border = new Rectangle(80, 80);

        @Override
        public String toString() {
            return "Tile{" +
                    "text=" + text +
                    '}';

        }

        public Tile() {
            RotateTransition rotate = new RotateTransition();
            rotate.setAxis(Rotate.Z_AXIS);
            rotate.setDuration(Duration.millis(800));
            rotate.setToAngle(90);
            rotate.setNode(border);
            rotate.play();

            border.setFill(Color.AZURE);
            border.setStroke(Color.BLACK);
            text.setFont(Font.font(62));
            setAlignment(Pos.CENTER);
            getChildren().addAll(border, text);

            setOnMouseClicked(event -> {
                int row = (int) getTranslateY() / 80;
                int col = (int) getTranslateX() / 80;

                if (!playable || getValue().equals("X") || getValue().equals("O")) {
                    return;
                }
                if (event.getButton() == MouseButton.PRIMARY) {
                    if (!turnX) {
                        return;
                    }
                    game.playMoveX(new Move(row, col));
                    drawX();
                    turnX = false;
                    checkState();

                } else if (event.getButton() == MouseButton.SECONDARY) {
                    if (turnX) {
                        return;
                    }
                    game.playMoveO(new Move(row, col));
                    drawO();
                    turnX = true;
                    checkState();
                }
            });
        }

        public String getValue() {
            return text.getText();
        }

        private void drawX() {
            text.setText("X");
        }

        private void drawO() {
            text.setText("O");
        }

        private void drawEmpty() {
            text.setText(" ");
        }

        public void setColor(Color color) {
            border.setFill(color);
        }
    }
}